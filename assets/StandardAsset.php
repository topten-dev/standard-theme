<?php
namespace topten\assets;

use yii\web\AssetBundle;

class StandardAsset extends AssetBundle
{
    public $sourcePath = '@vendor/topten-dev/standard-theme/web';

    public $css = [
        'css/style.css',
    ];

    public $js = [
        'js/app.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'topten\assets\Html5shiv',
    ];
}
