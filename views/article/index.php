<?php
/* @var $this yii\web\View */
$this->title = Yii::t('standard-theme', 'Articles')
?>
<div id="article-index">
    <h1><?php echo Yii::t('standard-theme', 'Articles') ?></h1>
    <?php echo \yii\widgets\ListView::widget([
        'dataProvider'=>$dataProvider,
        'pager'=>[
            'hideOnSinglePage'=>true,
        ],
        'itemView'=>'_item'
    ])?>
</div>